import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;
import eternal_cycle.EternalCycle;
import eternal_cycle.Bat;
import game_params.GameParams;
import atlas.Atlas;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    er: EternalCycle,
    cs: Bat,
    game_params: GameParams,
    atlasBoss: atlas.props.Boss,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
