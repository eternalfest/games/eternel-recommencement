package eternal_cycle;

import hf.entity.Player;
import hf.Entity;
import hf.FxManager;
import hf.Hf;
import hf.mode.Adventure;
import hf.levels.GameMechanics;
import merlin.IAction;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;

import eternal_cycle.actions.PopShield;
import eternal_cycle.actions.GiveBonus;
import better_script.Bombexpert;

@:build(patchman.Build.di())
class EternalCycle {
    @:diExport
    public var death_reset_and_stats(default, null): IPatch;
    @:diExport
    public var silence_wrong_music_at_game_begin(default, null): IPatch;
    @:diExport
    public var change_start_level_to_first_of_dim1(default, null): IPatch;
    @:diExport
    public var force_display_of_land_name_on_well0(default, null): IPatch;
    @:diExport
    public var display_land_name_of_well0_as_new_land(default, null): IPatch;

    @:diExport
    public var pop_shield(default, null): IAction;
    @:diExport
    public var give_bonus(default, null): IAction;
    @:diExport
    public var addBombExpert(default, null): IAction;

    public function new(): Void {
        this.death_reset_and_stats = Ref.auto(Player.killPlayer).before(function(hf: Hf, self: Player): Void {
            var bombs : Array<Entity> = self.game.getList(hf.Data.BOMB);
            for (bomb in bombs)
                bomb.destroy();

            var projectiles : Array<Entity> = self.game.getList(hf.Data.SHOOT);
            for (projectile in projectiles)
                projectile.destroy();
        });

        this.silence_wrong_music_at_game_begin = Ref.auto(Adventure.initGame).after(function(hf: Hf, self: Adventure): Void {
            self.playMusic(5);
        });

        this.change_start_level_to_first_of_dim1 = Ref.auto(Adventure.initWorld).after(function(hf: Hf, self: Adventure): Void {
            /* In case we don't even get to see the well, otherwise when saving the max level would be undefined. */
            self.dimensions[0].currentId = 0;

            /* Outside of the well we actually want to give it +1 compared to the level we actually want. */
            self.firstLevel = 1;

            self.currentDim = 1;
            self.world = self.dimensions[self.currentDim];
        });

        this.force_display_of_land_name_on_well0 = Ref.auto(GameMechanics.isVisited).wrap(function(hf: Hf, self: GameMechanics, old): Bool {
            /* A bit brutal but we never visit the same level twice. */
            if (self.currentId == 0)
                return false;
            return old(self);
        });

        this.display_land_name_of_well0_as_new_land = Ref.auto(FxManager.attachLevelPop).wrap(function(hf: Hf, self: FxManager, name: String, isNewLand: Bool, old): Void {
            old(self, name, true);
        });

        this.pop_shield = new PopShield();
        this.give_bonus = new GiveBonus();
        addBombExpert = new Bombexpert().action;
    }
}