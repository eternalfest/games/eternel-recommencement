package eternal_cycle.actions;

import hf.mode.GameMode;
import hf.entity.Player;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PopShield implements IAction {
    public var name(default, null): String = Obfu.raw("popShield");
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var game: GameMode = ctx.getGame();

        var players: Array<Player> = game.getPlayerList();
        for (player in players)
            player.unshield();

        return false;
    }
}