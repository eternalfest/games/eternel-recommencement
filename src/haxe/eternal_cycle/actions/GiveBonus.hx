package eternal_cycle.actions;

import hf.mode.GameMode;
import hf.entity.Player;
import hf.Hf;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class GiveBonus implements IAction {
    public var name(default, null): String = Obfu.raw("giveBonus");
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var bonus_per_life: Int = ctx.getOptInt(Obfu.raw("bonus")).or(20000);

        var game: GameMode = ctx.getGame();
        var hf: Hf = ctx.getHf();

        var players: Array<Player> = game.getPlayerList();
        for (player in players) {
            var bonus: Int = bonus_per_life * player.lives;
            game.fxMan.attachAlert(hf.Lang.get(35) + player.lives + " x " + hf.Data.formatNumber(bonus_per_life));
            player.getScore(player, bonus);
        }

        return false;
    }
}